# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

This is the repository for a bot created for a Discord channel. The bot is used to gather data from the pictures
/ links / reactions posted in the channel, and the data is analyzed and results are calculated from it.

The bot is online 24/7 through Heroku app, which hosts the bot for free and is accessible through Git.
Therefore the bot is all the time usable.

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
